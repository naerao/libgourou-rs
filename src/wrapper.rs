#[cxx::bridge]
pub mod wrapper {

    #[namespace = "gourou::rust"]
    unsafe extern "C++" {
        include!("libgourou-rs/include/wrapper.h");
        type FulfillmentWrapper;
        type GourouWrapper;

        fn new_wrapper(libgourou_dir: &CxxString) -> UniquePtr<GourouWrapper>;
        fn sign_in(self: Pin<&mut GourouWrapper>, adobe_id: &CxxString, password: &CxxString);
        fn activate_device(self: Pin<&mut GourouWrapper>);
        fn download(
            self: Pin<&mut GourouWrapper>,
            acsm_path: &CxxString,
            output_path: Pin<&mut CxxString>,
            resume: bool
        ) -> u8;
        fn remove_drm(self: Pin<&mut GourouWrapper>, drm_file: &CxxString, output: &CxxString, file_type: u8);
    }

}
