#include "libgourou-rs/include/wrapper.h"
#include "drmprocessorclientimpl.h"
#include "libgourou.h"
#include "fulfillment_item.h"
#include "libgourou_common.h"
#include <memory>
#include <iostream>

namespace gourou::rust {

GourouWrapper::GourouWrapper(const std::string& libgourou_dir) {
    client = new DRMProcessorClientImpl();
    processor = gourou::DRMProcessor::createDRMProcessor(
        client,
        false,
        libgourou_dir
    );
}

GourouWrapper::~GourouWrapper() {
    if(processor) delete processor;
    if(client) delete client;
}

void GourouWrapper::sign_in(const std::string& adobe_id, const std::string& password) {
    processor->signIn(adobe_id, password);
}

void GourouWrapper::activate_device() {
    processor->activateDevice();
}

uint8_t GourouWrapper::download(const std::string& acsm_path, std::string& output, bool resume) {
    FulfillmentItem* item = processor->fulfill(acsm_path);
    gourou::DRMProcessor::ITEM_TYPE type = processor->download(item, output, resume);
    return type;
}

void GourouWrapper::remove_drm(const std::string& drm_file, const std::string& output, uint8_t file_type) {
    processor->removeDRM(drm_file, output, static_cast<gourou::DRMProcessor::ITEM_TYPE>(file_type), nullptr, 0);
}

std::unique_ptr<GourouWrapper> new_wrapper(const std::string& libgourou_dir) {
    return std::make_unique<GourouWrapper>(libgourou_dir);
}

}
