/// Filetype of a downloaded file
pub enum FileType {
    Epub,
    Pdf
}

impl FileType {

    /// Get extension for filetype
    pub fn extension(&self) -> &'static str {
        match self {
            FileType::Epub => "epub",
            FileType::Pdf => "pdf",
        }
    }

}

impl TryFrom<u8> for FileType {
    type Error = ();

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(FileType::Epub),
            1 => Ok(FileType::Pdf),
            _ => Err(())
        }
    }
}

impl From<&FileType> for u8 {
    fn from(val: &FileType) -> Self {
        match &val {
            FileType::Epub => 0,
            FileType::Pdf => 1,
        }
    }
}
