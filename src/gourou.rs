use std::{path::Path, pin::Pin};

use super::{
    FileType,
    wrapper::wrapper
};

/// Safe bindings to libgourou
pub struct Gourou {
    inner: cxx::UniquePtr<wrapper::GourouWrapper>
}

impl Gourou {

    /// Create new Gourou object
    /// Can take a long time
    pub fn new(libgourou_dir: &Path) -> Self {
        let libgourou_dir_str = libgourou_dir.to_str().unwrap();
        cxx::let_cxx_string!(libgourour_dir_cpp = libgourou_dir_str);
        Self {
            inner: wrapper::new_wrapper(&libgourour_dir_cpp),
        }
    }

    /// Sign in with AdobeId and password
    pub fn sign_in(&mut self, adobe_id: &str, password: &str) {
        cxx::let_cxx_string!(adobe_id_cpp = adobe_id);
        cxx::let_cxx_string!(passord_cpp = password);
        self.inner.pin_mut().sign_in(&adobe_id_cpp, &passord_cpp);
        self.inner.pin_mut().activate_device();
    }

    /// Sign in anonymously
    pub fn sign_in_anonymous(&mut self) {
        self.sign_in("anonymous", "");
    }

    /// Download file from acsm
    pub fn download(&mut self, acsm: &Path, output: &Path) -> FileType {
        let path_str = acsm.to_str().unwrap();
        cxx::let_cxx_string!(acsm_cpp = path_str);
        let output_str = output.to_str().unwrap();
        cxx::let_cxx_string!(output_cpp = output_str);

        let item = self.inner.pin_mut().download(&acsm_cpp, output_cpp, false);
        return item.try_into().unwrap();
    }

    /// Remove drm from file
    pub fn remove_drm(&mut self, drm_file: &Path, output: &Path, file_type: &FileType) {
        let drm_file_str = drm_file.to_str().unwrap();
        cxx::let_cxx_string!(drm_file_cpp = drm_file_str);
        let output_str = output.to_str().unwrap();
        cxx::let_cxx_string!(output_cpp = output_str);
        match file_type {
            FileType::Epub => {
                self.inner.pin_mut().remove_drm(&drm_file_cpp, &drm_file_cpp, file_type.into());
                std::fs::rename(&drm_file, &output);
            },
            FileType::Pdf => {
                self.inner.pin_mut().remove_drm(&drm_file_cpp, &output_cpp, file_type.into());
                std::fs::remove_file(&drm_file);
            }
        }
    }

}
