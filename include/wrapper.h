#pragma once
#include "libgourou.h"
#include "fulfillment_item.h"
#include "drmprocessorclientimpl.h"
#include <memory>

namespace gourou::rust {

class FulfillmentWrapper {
private:
public:
    gourou::FulfillmentItem* inner;
    FulfillmentWrapper(gourou::FulfillmentItem* x);
    ~FulfillmentWrapper();
};

class GourouWrapper {
private:
    DRMProcessorClientImpl* client;
    DRMProcessor* processor;
public:
    GourouWrapper(const std::string& libgourou_dir);
    ~GourouWrapper();
    void sign_in(const std::string& adobe_id, const std::string& password);
    void activate_device();
    uint8_t download(const std::string& acsm_path, std::string& output, bool resume);
    void remove_drm(const std::string& drm_file, const std::string& output, uint8_t file_type);
};

std::unique_ptr<GourouWrapper> new_wrapper(const std::string& libgourou_dir);

}
