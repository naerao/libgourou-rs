fn main() {
    cxx_build::bridge("src/wrapper.rs")
        .include("include")
        .file("src/drmprocessorclientimpl.cpp")
        .file("src/wrapper.cpp")
        .compile("libgourou-rs");

    println!("cargo:rustc-link-lib=ssl");
    println!("cargo:rustc-link-lib=crypto");
    println!("cargo:rustc-link-lib=curl");
    println!("cargo:rustc-link-lib=zip");
    println!("cargo:rustc-link-lib=z");
    println!("cargo:rustc-link-lib=pugixml");
    println!("cargo:rustc-link-lib=gourou");
}
