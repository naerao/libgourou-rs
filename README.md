# libgourou-rs
Bad [rust](https://www.rust-lang.org/) bindings for
[libgourou](https://indefero.soutade.fr/p/libgourou/), made primarily for
[naerao](https://gitlab.com/naerao/naerao).

Includes modified version of [drmprocessorclientimpl.cpp](./src/drmprocessorclientimpl.cpp) and
header files from [libgourou](https://indefero.soutade.fr/p/libgourou/).

<!-- vim: textwidth=100 -->
