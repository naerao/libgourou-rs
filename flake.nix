{
  description = "Libgourou bindings for rust";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { nixpkgs, flake-utils, ... }:
  flake-utils.lib.eachDefaultSystem(system:
  let
    pkgs = nixpkgs.legacyPackages.${system};
  in
  {
    devShells.default = pkgs.mkShell {
      buildInputs = with pkgs; [
        rustc
        cargo

        clang
        libgourou
        pugixml
        openssl.dev
        curl.dev
        libzip.dev
        zlib.dev
      ];
    };
  });
}
